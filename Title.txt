Peleryna niewidka

Opis:
Peleryna−niewidka z angielskiego Cloak of Invisibility. Magiczny płaszcz, umożliwiający znikanie osoby, która się nią okryje. Peleryny−niewidki tkane są z sierści demimoza. Magicznego stworzenia, które potrafi stawać się niewidzialnym. Mogąrównież być zwykłymi płaszczami, nasyconymi zaklęciami zwodzącymi lub oślepiającymi. Wraz z upływem czasu takie peleryny-niewidki tracą swoje cudowne właściwości. Jedyną peleryną, która przez lata zachowała własności i czyniła właściciela całkowicie niewidzialnym. Jest płaszcz Ignotusa Peverella – jedno z trzech Insygniów Śmierci. Płaszcz ten skrywa w sobie dodatkowe właściwości. W przypadku połączenia z pozostałymi insygniami, staje się śmiercionośną bronią.

Instrukcja: 
1. Użytkowanie peleryny jest proste wystarczy ją założyć.  
2. Wielkość użytkownika nie ma znaczenia peleryna się dopasuje.
3. Proszę jej nie prać pod żadnym pozorem.
4. Przechowywać w miejscu z dala od dzieci.
5 Jest odporna na ogień ale za użytkownika nie odpowiadamy.
6. Gwarancja jest wieczysta.
7. Życzymy udanego korzystania.

Akapit 1

Wszystko zaczęło się w małym miasteczku o nazwie Darington. Pewien siedmioletni chłopiec otrzymał swój pierwszy prawdziwy prezent. Okazało się, że nie jest to zwykły prezent acz magiczny. Chłopiec o imieniu Garry to nasz bohater był zupełnie zaskoczony od kogo mógł dostał taki prezent. Okazało się następnego dnia, że przyszedł list z wyjaśnieniami.

Akapit 2

W liście Garry otrzymał informację, że jest to peleryna niewidka i otrzymał ją od swojego ojca chrzestnego. Ojciec chrzestny miał na imie John i napisał Garremu że peleryna jest w ich rodzinie od lat. Wyjaśnił dlaczego nie odzywał się przez tyle czasu ponieważ Garry nawet nie wiedział o jego istnieniu. Wujek przekazał w liście również informację na temat istnienia szkoły czarodziejów do której młody Garry ma się udać. Następnego dnia Garry z swoją peleryna już siedział w pociągu w drodze do szkoły z nowymi nadziejami na przyszłość.

Akapit 3

Garry poznał wiele osób w drodze do nowej szkoły nie mógł już się doczekać tego fantastycznego świata i sposób na użycie jego magicznego przedmotu. Już w pierwszy dzień szkoły wydarzyła się straszna rzecz, nauczyciel który miał uczyć Garrego zniknął w tajemniczych okolicznościach. Dyrektor szkoły zakazał uczniom wałęsania się po szkole po lekacjach wszyscy mieli siedzieć w swoich dormitoriach. Garry jednak się nie posłuchał nie dość, że uciekł z dormitorium do jeszcze z szkoły nikt go nie zauważył bo miał na sobie pelerynę niewidkę. Nasz bohater napotkał na swojej drodze dwóch nauczycieli którzy żywo rozmawiali o zniknięciu nauczyciela postanowił, że będzie ich śledzić.

Akapit 4

Chłopiec wiele długich chwil podążał za para nauczycieli, którzy najwidoczniej szukali zaginionego. Nagle zza drzew wychyliła się straszliwa istota o przerażająych oczach ziejąca ogniem. Istota ta zaatakowała nauczycieli którzy bardzo szybko i sprawnie zaczeli się bronić. Jeden z nich niestety upadł bez ruchu od uderzenia natomiast drugi nauczyciel uciekał przed ogniem potwora. W to wszystko wmieszał się mały Garry najpierw bezpiecznie ukrył pierwszego nauczyciela następnie drugiego obronił przed ogniem.

Akapit 5

Potwór odpuścił swój atak ale to nie wszystko zmienił się w czarodzieja, tym czarodziejem był właśnie zaginiony nauczycielu. Na szczęcie on sam nie wiedział że się zdemaskował gdyż Garry ukrył wszystkich pod peleryną. Zły czarodziej oddalił się a Garry zabrał nauczycieli do szkoły gdzie obaj zostali uleczeni. Następnego dnia dyrektor pochwalił postawę chłopca ale jednocześnie dał mu surową karę za złamanie zakazu. Dalsze losy chłopca i jego magicznego przedmiotu poznacie w następnej części.


